#include<fstream>
#include<iostream>
#include<string>

#include"dungeon.h"
int main(int argc, char **argv){
	//is there a file to copy map layout from?
	bool randGen = true;
	std::string fileName = "emptyfloor.txt";
	if(argc > 1){
		randGen = false;
		fileName = argv[1];
	}
	//create and run
	std::ifstream mapFile(fileName);
	Dungeon theGame = { mapFile, randGen };
	theGame.play();
	return 0;
}
