#include "mhoard.h"

MerchHoard::MerchHoard(Coordinate &p, Cell &c) : Treasure(p, c)
{}

int MerchHoard::getAmount() {
	return amount;
}

void MerchHoard::myEffect(PlayerRace &pl) {
	pl.getStats().addGold(amount);
	setInactive();
}

std::string MerchHoard::describeEffect() {
	return "With the merchant dead, you are free to take their loot. (+4 gold)\nYou feel, however, like someone now has a grudge against you.";
}

std::string MerchHoard::giveType() {
	return "a gold stash belonging to a dead merchant";
}
