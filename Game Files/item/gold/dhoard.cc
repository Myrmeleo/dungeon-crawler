#include "dhoard.h"
#include "../../character/enemyRace/dragon.h"

DragonHoard::DragonHoard(Coordinate &p, Cell &c) : Treasure(p, c)
{}

int DragonHoard::getAmount() {
	return amount;
}

void DragonHoard::addGuardian(Dragon &d) {
	guardian = &d;
}

void DragonHoard::notifyDeath() {
	guardian = nullptr;
}

void DragonHoard::myEffect(PlayerRace &pl) {
	if (!guardian) {
		pl.getStats().addGold(amount);
		setInactive();
	}
}

std::string DragonHoard::describeEffect() {
	if (guardian) {
		return "You try to take the gold, but the dragon ferociously beats you back.";
	}
	else {
		return "With the dragon dead, you are free to take its large pile of treasure for yourself. (+6 gold)";
	}
}

std::string DragonHoard::giveType() {
	return "a large gold pile belonging to a dragon";
}
