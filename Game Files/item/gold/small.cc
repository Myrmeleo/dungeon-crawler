#include "small.h"


Small::Small(Coordinate &p, Cell &c) : Treasure(p, c)
{}

int Small::getAmount() {
	return amount;
}

void Small::myEffect(PlayerRace &pl) {
	pl.getStats().addGold(amount);
	setInactive();
}

std::string Small::describeEffect() {
	return "You pick up a small amount of gold. Every bit counts. (+1 gold)";
}

std::string Small::giveType() {
	return "a small pile of gold";
}
