#include "normal.h"


Normal::Normal(Coordinate &p, Cell &c) : Treasure(p, c)
{}

int Normal::getAmount() {
	return amount;
}

void Normal::myEffect(PlayerRace &pl) {
	pl.getStats().addGold(amount);
	setInactive();
}

std::string Normal::describeEffect() {
	return "You pick up a fair amount of gold. (+2 gold)";
}

std::string Normal::giveType() {
	return "a moderate pile of gold";
}
