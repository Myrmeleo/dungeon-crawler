#include "wounddef.h"

bool WoundDEF::known = false;

WoundDEF::WoundDEF(Coordinate &p, Cell &c) : Potion(p, c)
{}

bool WoundDEF::isKnown() {
	return known;
}

void WoundDEF::becomeKnown() {
	known = true;
}

void WoundDEF::myEffect(PlayerRace &pl) {
	becomeKnown();
	pl.getStats().addDEF(-5);
	setInactive();
}

std::string WoundDEF::describeEffect() {
	return "You drink the potion. You feel your bones wearing away. (-5 to DEF)";
}

std::string WoundDEF::giveType() {
	if (isKnown()) {
		return "a potion of frailty (-5 to DEF)";
	}
	else {
		return "an unknown potion";
	}
}
