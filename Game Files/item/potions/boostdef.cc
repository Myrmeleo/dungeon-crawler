#include "boostdef.h"

bool BoostDEF::known = false;

BoostDEF::BoostDEF(Coordinate &p, Cell &c) : Potion(p, c)
{}

bool BoostDEF::isKnown() {
	return known;
}

void BoostDEF::becomeKnown() {
	known = true;
}

void BoostDEF::myEffect(PlayerRace &pl) {
	becomeKnown();
	pl.getStats().addDEF(5);
	setInactive();
}

std::string BoostDEF::describeEffect() {
	return "You drink the potion. You feel fortified. (+5 to DEF)";
}

std::string BoostDEF::giveType() {
	if (isKnown()) {
		return "a potion of resilience (+5 to DEF)";
	}
	else {
		return "an unknown potion";
	}
}
