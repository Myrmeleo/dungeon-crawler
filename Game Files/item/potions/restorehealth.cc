#include "restorehealth.h"

bool RestoreHealth::known = false;

RestoreHealth::RestoreHealth(Coordinate &p, Cell &c) : Potion(p, c)
{}

bool RestoreHealth::isKnown() {
	return known;
}

void RestoreHealth::becomeKnown() {
	known = true;
}

void RestoreHealth::myEffect(PlayerRace &pl) {
	becomeKnown();
	pl.getStats().addHP(10);
	pl.getBaseStats().addHP(10);
	setInactive();
}

std::string RestoreHealth::describeEffect() {
	return "You drink the potion. You feel revived. (+10 to HP)";
}

std::string RestoreHealth::giveType() {
	if (isKnown()) {
		return "a healing potion (+10 to HP)";
	}
	else {
		return "an unknown potion";
	}
}
