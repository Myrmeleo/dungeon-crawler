#include "item.h"


Item::Item(Coordinate &p, Cell &c) : position(&p), cell(&c)
{}

Item::~Item(){
}

void Item::myEffect(PlayerRace &pl) {
	(void)pl;
}

std::string Item::giveType() {
	return "a thing";
}

char Item::getAvatar() {
	return '.';
}

Coordinate Item::getPos(){
	return *(cell->getPos());
}

std::string Item::describeEffect() {
	return "You use the item, but you're not sure what it did.";
}

bool Item::isSteppableByPlayer(){
	return false;;
}

bool Item::getActive(){
	return active;
}

void Item::setInactive(){
	//remove references
	cell->removeItem();
}
