#ifndef ___DUNGEON___H___
#define ___DUNGEON___H___

#include <vector>
#include <string>
#include <fstream>
#include "map/Floor/floor.h"
#include "display/textDisplay.h"
#include "direction.h"
#include "character/character.h"

class Dungeon{
	//read from file, or make randomly?
	bool randGen;
	TextDisplay *td;
	//chars to display
	std::vector<std::vector<std::string>> FloorMaps;
	Floor *currentFloor;
	//stairs location
	Coordinate *nextFloorLoc;
	PlayerRace *player;
	std::string playerType;
	int level;
	public:
	Dungeon(std::ifstream &mapFile, bool whatGen);
	~Dungeon();
	//command interpreter, run continuously
	bool play();	
	//print an instruction guide
	std::string gameManual();
	//prompt user at start of new game
	void characterSelect();
	//update enemy positions
	void moveEnemies();
	//generate new floor
	void nextFloor();
	//display prompt when user wins or dies
	void victorySplash();
	//generate new floors, user picks new player
	void reset();
};

#endif
