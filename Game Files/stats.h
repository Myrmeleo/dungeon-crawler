#ifndef STATS_H
#define STATS_H
//combat, items will change affect characters by affecting values of this object

const int minHP = 0;
const int minATK = 0;
const int minDEF = 0;

class Stats {
	int HP;
	int maxHP;
	int attack;
	int defence;
	int gold;
public:
	//all of this is self-explanatory
	Stats(int hp, int mhp, int a, int d);
	Stats(Stats &s);
	int getHP();
	int getMaxHP();
	int getATK();
	int getDEF();
	int getGold();
	void addHP(int amt);
	void addATK(int amt);
	void addDEF(int amt);
	void addGold(int amt);
};


#endif
