#ifndef COORDINATE_H
#define COORDINATE_H
#include <string>
//tracks the position of a cell/character/item
//top left cell of the game area is (0,0), right and down are positive directions

class Coordinate {
	int X;
	int Y;
public:
	Coordinate(int x = 0, int y = 0);
	int getX();
	int getY();
	bool operator==(Coordinate other);
};

//pos1 has more X than pos2
bool isEast(Coordinate &pos1, Coordinate &pos2);
//pos1 has more Y than pos2
bool isNorth(Coordinate &pos1, Coordinate &pos2);
//same Y value
bool equalNorth(Coordinate &pos1, Coordinate &pos2);
//same X value
bool equalEast(Coordinate &pos1, Coordinate &pos2);
//can pos2 be reached from pos1 by heading in dir?
bool inDir(std::string dir, Coordinate &pos1, Coordinate &pos2);
//is pos2 neighbour of pos1?
bool oneTileAway(Coordinate obj1, Coordinate obj2);
#endif
