#include "chamber.h"
#include <algorithm>

#include "../../character/enemyRace/dragon.h"
#include "../../character/enemyRace/dwarf.h"
#include "../../character/enemyRace/elf.h"
#include "../../character/enemyRace/halfling.h"
#include "../../character/enemyRace/human.h"
#include "../../character/enemyRace/merchant.h"
#include "../../character/enemyRace/orc.h"

#include "../../item/potions/boostatk.h"
#include "../../item/potions/boostdef.h"
#include "../../item/potions/poisonhealth.h"
#include "../../item/potions/restorehealth.h"
#include "../../item/potions/woundatk.h"
#include "../../item/potions/wounddef.h"

#include "../../item/gold/dhoard.h"
#include "../../item/gold/mhoard.h"
#include "../../item/gold/normal.h"
#include "../../item/gold/small.h"

#include<ctime>
#include<stdlib.h>
#include<random>
#include<iostream>
#include <cstdlib>

using namespace std;

Chamber::Chamber(TextDisplay *td, vector<Coordinate*> &reqCells, const vector<Cell*> &theCells):myTd{td}{
	for(auto i : reqCells){
		for(auto cell : theCells){
			if((*i) == *(cell->getPos())){
				cells.emplace_back(cell);
				break;
			}
		}
	}
}


Coordinate *Chamber::genLoc(char whatChar){
	bool occupied = true;
	
	while(occupied){
		int whichLoc = rand() % cells.size();
		//Coordinate candidateLoc = *(cells.at(whichLoc)->getPos();
		occupied = cells.at(whichLoc)->isOccupied();
		
		if(!occupied){
			//cells.at(whichLoc)->setOccupy(true);
			cells.at(whichLoc)->setDisplayChar(whatChar);
			
			return cells.at(whichLoc)->getPos();
		}
	}
	//should not be possible
	return new Coordinate{-1, -1};
}

Item *Chamber::genPotionLoc() {
	Coordinate *newLoc = genLoc('.');
	Cell *whichCell = getCellAt(*newLoc);
	int potionToSpawn = rand() % 6;
	if (potionToSpawn == 0) {
		BoostATK *potion = new BoostATK(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
	else if (potionToSpawn == 1) {
		WoundATK *potion = new WoundATK(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
	else if (potionToSpawn == 2) {
		BoostDEF *potion = new BoostDEF(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
	else if (potionToSpawn == 3) {
		WoundDEF *potion = new WoundDEF(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
	else if (potionToSpawn == 4) {
		PoisonHealth *potion = new PoisonHealth(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
	else {
		RestoreHealth *potion = new RestoreHealth(*newLoc, *whichCell);
		whichCell->setItem(potion);
		return potion;
	}
}

Item *Chamber::genGoldLoc(vector<Character *> &enemies) {
	Coordinate *newLoc = genLoc('.');
	Cell *whichCell = getCellAt(*newLoc);
	int goldToSpawn = rand() % 8;
	if (goldToSpawn <= 4) {
		Normal *gold = new Normal(*newLoc, *whichCell);
		whichCell->setItem(gold);
		return gold;
	}
	else if (goldToSpawn <= 6) {
		Small *gold = new Small(*newLoc, *whichCell);
		whichCell->setItem(gold);
		return gold;
	}
	else {
		DragonHoard *gold = new DragonHoard(*newLoc, *whichCell);
		
		
		bool fail = false;
		while(!fail){
			DragonHoard *gold = new DragonHoard(*newLoc, *whichCell);
		
			whichCell->setItem(gold);
			//try to do it randomly
			Cell *neighbour = whichCell->getRandomNeighbour();
			int ticker = 1;
			while(neighbour->isOccupied()){
				neighbour = whichCell->getRandomNeighbour();
				
				//try 20 times
				if(ticker == 20){
					//find next availble pos
					for (auto i : whichCell->getNeighbours()) {
						if (!i->isOccupied()) {
							neighbour = i;
							break;
						}
					}
					//failed to find a spot for dragon
					fail = true;
					break;
				}
				++ticker;
			}
			
			Dragon *guard = new Dragon(*neighbour->getPos(), *neighbour, *gold);
			neighbour->setCharacter(guard);
			enemies.emplace_back(guard);
			break;
		}
		return gold;
	}
}

Character *Chamber::genMonsterLoc() {
	Coordinate *newLoc = genLoc('.');
	Cell *whichCell = getCellAt(*newLoc);
	int enemyToSpawn = rand() % 18;
	if (enemyToSpawn <= 3) {
		Human *enemy = new Human(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	else if (enemyToSpawn <= 6) {
		Dwarf *enemy = new Dwarf(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	else if (enemyToSpawn <= 11) {
		Halfling *enemy = new Halfling(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	else if (enemyToSpawn <= 13) {
		Elf *enemy = new Elf(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	else if (enemyToSpawn <= 15) {
		Orc *enemy = new Orc(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	else {
		Merchant *enemy = new Merchant(*newLoc, *whichCell);
		whichCell->setCharacter(enemy);
		return enemy;
	}
	//Dragons are spawned using genGoldLoc, since they are tied to their gold piles
}


Cell *Chamber::genPlayerLoc(){
	
	Coordinate *tmp = genLoc('.');
	
	for(auto i : cells){
		if(*(i->getPos()) == *tmp){
			return i;
		}
	}
	
	//should not be possible
	return nullptr;
}


bool Chamber::ValidMove(Coordinate coord){
	Cell *candCell = nullptr;
	for (auto cell : cells) {
		//cout<<"asdfasdfasdf"<<endl;
		if (*(cell->getPos()) == coord) {
			candCell = cell;
		}
	}
	/*
	bool test = cells.end() == find_if(cells.begin(), cells.end(),
	[&coord](Cell *inCell)
	{return inCell->getPos().getX() == coord.getX() && inCell->getPos().getY() == coord.getY(); });
	*/

	if (candCell) {
		return !(candCell->isOccupied());
	}

	else {
		return false;
	}	
}

Coordinate *Chamber::genStairLoc(){
	Coordinate *tmp = genLoc('/');
	
	cout<<"( "<<tmp->getX()<< " "<<tmp->getY() << " )"<<endl;
	return new Coordinate(tmp->getX(), tmp->getY());
	
}

Cell *Chamber::getCellAt(Coordinate &pos){
	for(auto cell : cells){
		if(*(cell->getPos()) == pos){
			return cell;
		}
	}
	return nullptr;
}

bool Chamber::moveEnemies() {
	return true;
}

