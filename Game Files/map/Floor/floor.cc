#include "floor.h"

#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<iostream>

#include "../../character/enemyRace/dragon.h"
#include "../../character/enemyRace/dwarf.h"
#include "../../character/enemyRace/elf.h"
#include "../../character/enemyRace/halfling.h"
#include "../../character/enemyRace/human.h"
#include "../../character/enemyRace/merchant.h"
#include "../../character/enemyRace/orc.h"

#include "../../item/potions/boostatk.h"
#include "../../item/potions/boostdef.h"
#include "../../item/potions/poisonhealth.h"
#include "../../item/potions/restorehealth.h"
#include "../../item/potions/woundatk.h"
#include "../../item/potions/wounddef.h"

#include "../../item/gold/dhoard.h"
#include "../../item/gold/mhoard.h"
#include "../../item/gold/normal.h"
#include "../../item/gold/small.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool invalidTile(char test){return (test == ' ' || test == '|' || test == '-' || test == '#' || test == '+'); }

bool detAddCoord(vector<Coordinate*> &chamberCoords, vector<string> plan, int xOff, int yOff, Coordinate *i){
	if(!invalidTile(plan.at(i->getX() + xOff)[i->getY() + yOff])){
		if(chamberCoords.end() == find_if(chamberCoords.begin(), chamberCoords.end(), 
			[&](Coordinate *inCoord){return inCoord->getX() == i->getX() + xOff && inCoord->getY() == i->getY() + yOff; })){
			Coordinate *newC = new Coordinate{ i->getX() + xOff, i->getY() + yOff };
			chamberCoords.emplace_back(newC);
			return true;
		}
	}
	return false;
}

//gives the cells in a chamber
void reqCellsInChamber(Coordinate *coord, vector<string> plan, vector<Coordinate*> &chamberCoords){
	chamberCoords.emplace_back(coord);
	bool newTile = true;
	while(newTile){
		newTile = false;
		for (int i = 0; i < chamberCoords.size(); ++i) {
			newTile = detAddCoord(chamberCoords, plan, 1, 0, chamberCoords.at(i)) || newTile;
			newTile = detAddCoord(chamberCoords, plan, 0, 1, chamberCoords.at(i)) || newTile;
			newTile = detAddCoord(chamberCoords, plan, -1, 0, chamberCoords.at(i)) || newTile;
			newTile = detAddCoord(chamberCoords, plan, 0, -1, chamberCoords.at(i)) || newTile;
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Floor::~Floor() {
	for (auto a : items) {
		delete a;
	}
	for (auto b : enemies) {
		delete b;
	}
	for (auto c : cells) {
		delete c;
	}
	for (auto d : chambers) {
		delete d;
	}
}

void Floor::genPotionLoc() {
	for (int i = 0; i < maxPotions; ++i) {
		int whichChamber = rand() % chambers.size();
		items.emplace_back(chambers.at(whichChamber)->genPotionLoc());
	}
}

void Floor::genGoldLoc() {
	for (int i = 0; i < maxTreasure; ++i) {
		int whichChamber = rand() % chambers.size();
		items.emplace_back(chambers.at(whichChamber)->genGoldLoc(enemies));
	}
}

void Floor::genMonsterLoc() {
	for (int i = 0; i < maxEnemies; ++i) {
		int whichChamber = rand() % chambers.size();
		enemies.emplace_back(chambers.at(whichChamber)->genMonsterLoc());
	}
}

//gives me a correct floor char, either '#', '+', '/' or '.'
char getFloorChar(char a){
	if(a == '+' || a == '#' || a == '\\'){ 
		return a; //passages
	}
	else return '.';
}

Floor::Floor(std::vector<std::string> plan, TextDisplay *newDisp, bool whatGen, PlayerRace *player, Coordinate *stairCoord):td{newDisp},randGen{whatGen}{
	int i = 0;
	//make cells
	for(auto row : plan){
		for(int j = 0; row[j]; ++j){
			//if(row[j] == '#' || row[j] == '+' || row[j] == '.') passages.emplace_back(new Cell(td, row[j], Coordinate{i,j}));
			if(!(row[j] == ' ' || row[j] == '|' || row[j] == '-')){
				
				cells.emplace_back(new Cell(td, getFloorChar(row[j]), new Coordinate(i,j)));
			}
		}
		++i;
	}
	//adds the neighbours
	for(auto curCell : cells){
		for(auto neighbourCell : cells){
			if(oneTileAway(*(curCell->getPos()), *(neighbourCell->getPos()))){
				curCell->addNeighbour(neighbourCell);
			}
		}
	}
	i = 0;
	for(auto row : plan){
		for(int j = 0; row[j]; ++j){
			if(!(invalidTile(row[j]))){
				bool newChamber = true;
				for(auto oldChamber : chambers){
					if(oldChamber->ValidMove(Coordinate{i,j})){
						newChamber = false;
					}
				}
				//make chambers
				if(newChamber){
					vector<Coordinate *> reqCells;
					Coordinate *newCoord = new Coordinate(i, j);
					try{
						reqCellsInChamber(newCoord, plan, reqCells);
						chambers.emplace_back(new Chamber(td, reqCells, cells));
					} catch (...) {

					}
				}
			}
		}
		++i;
	}
	//place monster, player, stairwell etc
	if(randGen){ //make a random generation
		Cell * where = genPlayerLoc();
		player->changeCell(where);
		where->setCharacter(player);
		nextFloorLoc = genStairCase();
		genFloor();
	}
	else{ //readin from a file
		readEntities(plan, player, stairCoord);
	}
}

Cell *Floor::genPlayerLoc() {
	int whichChamber = rand() % chambers.size();
	spawnRoom = whichChamber;
	return chambers.at(whichChamber)->genPlayerLoc();
}

void Floor::genFloor() {
	genPotionLoc();
	genGoldLoc();
	genMonsterLoc();
}

#include<iostream>
Coordinate *Floor::genStairCase() {
	int whichChamber = rand() % chambers.size();
	while (whichChamber == spawnRoom) {
		whichChamber = rand() % chambers.size();
	}
	Coordinate *tmp = chambers.at(whichChamber)->genStairLoc();
	
	return tmp;
}

Coordinate *Floor::getStairLoc() {
	return nextFloorLoc;
}

bool Floor::ValidMove(Coordinate coord) {
	for (auto i : cells) {
		if (*(i->getPos()) == coord) {
			return true;
		}
	}
	int a = 0;
	for (auto chamber : chambers) {
		//cout<<"asfd"<<a<<"\n";
		if (chamber->ValidMove(coord)) {
			//cout<<"asfd"<<a<<"\n";
			//chamber->printChamber();
			return true;
		}
		++a;
	}
	return false;
}

void Floor::moveEnemies(){
	//for(auto i : chambers){
	//	i->moveEnemies();
	//}
	for(auto i : enemies){
		i->move();
	}
}


Cell *Floor::getCellAt(Coordinate where){
	for(auto i : cells){
		if(*(i->getPos()) == where){
			return i;
		}
	}
	return nullptr;
}

void setUpCharInCell(Character *someThing, Cell* cell, vector<Character *> &enemies){
	enemies.emplace_back(someThing);
	cell->setCharacter(someThing);
}
void setUpCharInCell(Item *someThing, Cell* cell, vector<Item *> &items){
	items.emplace_back(someThing);
	cell->setItem(someThing);
}

void Floor::readEntities(const std::vector<std::string> &plan, PlayerRace *player, Coordinate *stairwell){
		for (int i = 0; i < static_cast<int>(plan.size()); ++i){
			for(int j = 0; j < static_cast<int>(plan.at(i).size()); ++j){
				Cell *curCell = getCellAt(Coordinate{i, j});
				Coordinate tmp{-1, -1};
				switch(plan.at(i)[j]){
					//Player and stairwell
					case 64: //playerloc '@'
						
						player->changeCell(curCell);
						curCell->setCharacter(player);
						break;
					case '\\': //stairwell '\'
						
						stairwell = new Coordinate(i, j); //decouple
						break;
					//enemies ##########################################################
					case 'H': //Human
						setUpCharInCell(new Human(tmp, *curCell), curCell, enemies);
						break;
					case 'W'://Dwarf
						setUpCharInCell(new Dwarf(tmp, *curCell), curCell, enemies);
						break;
					case 'E'://elf
						setUpCharInCell(new Elf(tmp, *curCell), curCell, enemies);
						break;
					case 'O'://orc
						setUpCharInCell(new Orc(tmp, *curCell), curCell, enemies);
						break;
					case 'M'://merchant
						setUpCharInCell(new Merchant(tmp, *curCell), curCell, enemies);
						break;
					//potions ##########################################################
					case '0': //Restore health
						setUpCharInCell(new RestoreHealth(tmp, *curCell), curCell, items);
						break;
					case '1':// boost atk
						setUpCharInCell(new BoostATK(tmp, *curCell), curCell, items);
						break;
					case '2'://boost BoostDEF
						setUpCharInCell(new BoostDEF(tmp, *curCell), curCell, items);
						break;
					case '3'://poison health
						setUpCharInCell(new PoisonHealth(tmp, *curCell), curCell, items);
						break;
					case '4'://wound atk
						setUpCharInCell(new WoundATK(tmp, *curCell), curCell, items);
						break;
					case '5'://wound def
						setUpCharInCell(new WoundDEF(tmp, *curCell), curCell, items);
						break;
					//gold ##########################################################
					case '6':// normal gold pile
						setUpCharInCell(new Normal(tmp, *curCell), curCell, items);
						break;
					case '7'://small hoard
						setUpCharInCell(new Small(tmp, *curCell), curCell, items);
						break;
					case '8'://merchant hoard
						setUpCharInCell(new MerchHoard(tmp, *curCell), curCell, items);
						break;
					case '9'://dragonhoard and dragon, as dragon cannot be independent of its hoard but hoard can
						//required to be outside in this case
						Dragon *newDragon = nullptr; //in case of no dragon
						DragonHoard *newTreasure = new DragonHoard(tmp, *curCell);
						
						setUpCharInCell(newTreasure, curCell, items);
						
						//hold location of dragon
						int dragonX = i;
						int dragonY = j;
						
						//search for the dragon
						if(plan.at(i - 1)[j] == 'D'){ //no
							--dragonX;
						}
						else if(plan.at(i - 1)[j + 1] == 'D'){ //ne
							--dragonX;
							++dragonY;
						}
						else if(plan.at(i - 1)[j - 1] == 'D'){ //nw
							--dragonX;
							--dragonY;
						}
						else if(plan.at(i)[j + 1] == 'D'){ //ea
							++dragonY;
						}
						else if(plan.at(i)[j - 1] == 'D'){ //we
							--dragonY;
						}
						else if(plan.at(i + 1)[j] == 'D'){ //so
							++dragonX;
						}
						else if(plan.at(i + 1)[j + 1] == 'D'){ //se
							++dragonX;
							++dragonY;
						}
						else if(plan.at(i + 1)[j - 1] == 'D'){ //sw
							++dragonX;
							--dragonY;
						}
						else{//no dragon
							dragonX = -1;
						}
						
						if(dragonX >= 0){
							
							Cell *dragCell = getCellAt(Coordinate{dragonX, dragonY});
							newDragon = new Dragon(tmp, *dragCell, *newTreasure);
							setUpCharInCell(newDragon, dragCell, enemies);
							newTreasure->addGuardian(*newDragon);
						}
						
						break;
				}
			}
		}
}

std::string Floor::refresh(){
	//cleans up any dead chars or items
	cleanup();
	std::string goldFeedback = "";
	for(auto cell : cells){
		goldFeedback += cell->autoLootGold();
		cell->alertDisplayOfChange();
	}
	return goldFeedback;
}

std::string Floor::reactEnemies(Character *character){
	std::string attackSeq = "";
	for(auto i : enemies){
		attackSeq += i->attack(*character);
	}
	return attackSeq;
}


//for now this has no purpose
void Floor::cleanup(){
	
}

