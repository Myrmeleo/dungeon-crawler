#ifndef ___CHAMBER___H___
#define ___CHAMBER___H___

#include <vector>
#include <string>

#include "../../coordinate.h"
#include "../Cell/cell.h"
#include "../../display/textDisplay.h"
#include "../../character/character.h"
#include "../../item/item.h"

#include "../../character/enemyRace/enemyrace.h"

/*
class Item;
class EnemyRace;
class PlayerRace;
class Item;
class TextDisplay;
*/

class Chamber{
	bool randGen;
	std::vector<Cell*> cells;
	TextDisplay *myTd;
public:
	Chamber(TextDisplay *td, std::vector<Coordinate*> &reqCells, const std::vector<Cell*> &theCells);
	//random coordinate generator, coordinate is in chamber
	Coordinate *genLoc(char whatChar);
	//place potion
	Item *genPotionLoc();
	//place gold
	Item *genGoldLoc(std::vector<Character *> &enemies);
	//place enemy
	Character *genMonsterLoc();
	//place player
	Cell *genPlayerLoc();
	//place stair
	Coordinate *genStairLoc();
	Cell *getCellAt(Coordinate &pos);
	//cell at coordinate is not occupied of out of room
	bool ValidMove(Coordinate coord);
	//random movement
	bool moveEnemies();

};

int trueRand();
#endif
