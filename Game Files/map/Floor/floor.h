#ifndef ___FLOOR___H___
#define ___FLOOR___H___

#include "../Cell/cell.h"
#include "chamber.h"
#include "../../coordinate.h"
#include "../../display/textDisplay.h"
#include "../../character/character.h"

#include<vector>
#include<string>

class Item;
class EnemyRace;
class PlayerRace;

const int maxEnemies = 20;
const int maxPotions = 10;
const int maxTreasure = 10;

class Floor{
	TextDisplay *td;
	//read floor layout from file, or randomly generate it?
	bool randGen;
	
	std::vector<Cell*> cells;
	std::vector<Item*> items;
	std::vector<Character*> enemies;
	std::vector<Chamber*> chambers;
	
	int spawnRoom = 0;
	Coordinate spawnLoc;
	Coordinate *nextFloorLoc;
	//generate items and characters
	void genPotionLoc();
	void genGoldLoc();
	void genMonsterLoc();
public:
	Floor(std::vector<std::string> plan, TextDisplay *td, bool whatGen, PlayerRace *player, Coordinate *stairCoord);
	~Floor();
	//place player
	Cell *genPlayerLoc();
	//make chambers, call private generators
	void genFloor();
	//make stairs
	Coordinate *genStairCase();
	Coordinate *getStairLoc();
	//call validmove() for the right chamber
	bool ValidMove(Coordinate coord);
	//random movement
	void moveEnemies();
	Cell *getCellAt(Coordinate where);
	//get state of characters/items, determine interaction between characters and characters/items
	void readEntities(const std::vector<std::string> &plan, PlayerRace *player, Coordinate *stairwell);
	//get new game state after character/item state changes
	std::string refresh();
	//enemies will attack player
	std::string reactEnemies(Character *character);
	void cleanup();
};

void reqCellsInChamber(Coordinate *coord, std::vector<std::string> plan, std::vector<Coordinate*> &chamberCoords);

#endif
