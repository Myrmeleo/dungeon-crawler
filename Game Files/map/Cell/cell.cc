#include "cell.h"
#include "../../display/textDisplay.h"
#include "../../character/character.h"
#include "../../item/item.h"
#include "../../item/gold/treasure.h"

using namespace std;

Cell::Cell():position{nullptr},dispChar{0},td{nullptr}{}

Cell::Cell(TextDisplay *display, char type, Coordinate *coord):position{coord},dispChar{type},td{display}{
	
}

Cell::~Cell() {
	delete position;
}

Coordinate *Cell::getPos(){
	return position;
}

char Cell::getDisplayChar(){
	return dispChar;
}

void Cell::setDisplayChar(char a){
	dispChar = a;
}

Item *Cell::getItem(){
	return itemHere;
}

Character *Cell::getCharacter(){
	return characterHere;
}

void Cell::setItem(Item *item){
	itemHere = item;
}

void Cell::setCharacter(Character *character){
	characterHere = character;
}

void Cell::killCharacter() {
	//deletion handled by floor
	characterHere = nullptr;
}

void Cell::removeItem() {
	//deletion handled by floor
	itemHere = nullptr;
}

bool Cell::isOccupied() {
	return itemHere || characterHere || dispChar == '/';
}

void Cell::addNeighbour(Cell *neighbour){
	neighbours.emplace_back(neighbour);
}

std::vector<Cell *> &Cell::getNeighbours() {
	return neighbours;
}

void Cell::alertDisplayOfChange(){
	char tmp;
	//set to default dispChar
	td->notify(*this);
	
	//overwrite to item dispChar
	if(itemHere){
		tmp = dispChar;
		dispChar = itemHere->getAvatar();
		td->notify(*this);
		dispChar = tmp;
	}
	//if there was gold and a player want to ovewrite gold
	if(characterHere){
		tmp = dispChar;
		dispChar = characterHere->getAvatar();
		td->notify(*this);
		dispChar = tmp;
	}
}

void Cell::send(string dir){
	
	bool changed = false;
	for(auto cell : neighbours){
		changed = false;
		if(inDir(dir, *(this->getPos()), *(cell->getPos()))){
			changed = cell->receive(characterHere);
			
			//character moved
			if(changed){
				
				characterHere = nullptr;
			}
			return;
		}
	}
}

bool Cell::receive(Character *myChar){
	
	//case we have a playerRace and a Gold
	//note never the case we have an enemy and item in the same spot
	//so this is safe
	if(itemHere){
		if(myChar->isPlayable() && itemHere->isSteppableByPlayer()){ 
			characterHere = myChar;
			characterHere->changeCell(this);
			return true;
		}
	}
	if(dispChar == '/' && myChar->isPlayable()){//allow player to step on a staircase
		characterHere = myChar;
		characterHere->changeCell(this);		
		return true; 
	 }
	if(dispChar == '+' && !(myChar->isPlayable())){ //enemies can't enter passages
		return false;
	}
	if(!isOccupied()){
		characterHere = myChar;
		characterHere->changeCell(this);		
		return true;
	}
	//nothing changed
	return false;
}

Item *Cell::getItemInDir(std::string dir){
	for(auto cell : neighbours){
		if(inDir(dir, *(this->getPos()), *(cell->getPos()))){
			return cell->getItem();
		}
	}
	
	return nullptr;
}
Character *Cell::getCharacterInDir(std::string dir){
	for(auto cell : neighbours){
		if(inDir(dir, *(this->getPos()), *(cell->getPos()))){
			return cell->getCharacter();
		}
	}
	
	return nullptr;
}


std::string Cell::autoLootGold(){
	//guarenteed that this will be a player and a treasure as
	// this is the combination that allows two things on one tile
	std::string goldFeedback = "";
	if(characterHere && itemHere){
		goldFeedback += itemHere->describeEffect() + ".\n";
		itemHere->myEffect(*static_cast<PlayerRace*>(characterHere));
	}
	return goldFeedback;
}

Cell *Cell::getRandomNeighbour(){
	return neighbours.at(rand() % neighbours.size());
}
