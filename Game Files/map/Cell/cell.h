#ifndef CELL_H
#define CELL_H

#include "../../coordinate.h"

#include <vector>
#include <string>

class Item;
class Character;
class Treasure;
class PlayerRace;
class TextDisplay;

enum class CellType{HorizontalWall, VerticalWall, Tile, Doorway, Passage, Empty};

class Cell{
	Coordinate *position;
	char dispChar;
	CellType type;
	Item *itemHere = nullptr;
	Character *characterHere = nullptr;
	Treasure *treasureHere;
	//to notify td of a change
	TextDisplay *td;
	std::vector<Cell *> neighbours;
public:
	Cell();
	Cell(TextDisplay *display, char type, Coordinate *coord);
	~Cell();
	Coordinate *getPos();
	//for textdisplay, char depends on function and state of cell (tile? wall? occupied by character/item?)
	char getDisplayChar();
	void setDisplayChar(char a);
	//accessors/mutators
	Item *getItem();
	Character *getCharacter();
	void setItem(Item *);
	void setCharacter(Character *);
	//delete pointers and set to nullptr
	void killCharacter();
	void removeItem();
	//pointer not null?
	bool isOccupied();
	//used on initialization
	void addNeighbour(Cell *neighbour);
	std::vector<Cell *> &getNeighbours();
	//when a character/item changes
	void alertDisplayOfChange();
	//move character away
	void send(std::string dir);
	//character comes on
	bool receive(Character *myChar);
	void sendPlayer(std::string dir);
	bool receivePlayer(Character *myChar);
	//call on the neighbour for their pointer
	Item *getItemInDir(std::string dir);
	Character *getCharacterInDir(std::string dir);
	//remove looted gold
	std::string autoLootGold();
	Cell *getRandomNeighbour();
	
};
#endif


