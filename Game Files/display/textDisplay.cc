#include "textDisplay.h"
#include "../coordinate.h"
#include <iostream>
using namespace std;

TextDisplay::TextDisplay(vector<string> newDisplay):display{newDisplay}{
	
}

void TextDisplay::print(){
	cout<<"CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K CC3K"<<endl;
	for(auto i : display){
		cout << i << endl;
	}
}

void TextDisplay::notify(Cell &fromWho){
	Coordinate *where = fromWho.getPos();
	display.at(where->getX())[where->getY()] = fromWho.getDisplayChar();
}

void TextDisplay::load(vector<string> newDisplay){
	display = newDisplay;
}
