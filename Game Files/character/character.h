#ifndef CHARACTER_H
#define CHARACTER_H
#include "../stats.h"
#include "../coordinate.h"
#include "../map/Cell/cell.h"
#include <math.h>

class Vampire;
class Goblin;
class Drow;
class Elf;
class Orc;
class Dwarf;

char whatDir(int key);

class Character {
	
	
	Coordinate *position;
	
protected:
	Stats *stats;
	//stats will reset to base values at the end of the floor
	//(permanent modifiers will modify baseStats in addition to stats)
	Stats *baseStats;
	//the cell the character is standing on
	Cell *cell;
	//does the character alive
	bool active = true;
	
	//determines damage
	void damageTaken(Character &from);
public:
	Character(Stats &s, Stats &bs, Coordinate &p, Cell &c);
	virtual ~Character();// = 0;
	virtual void specialEffect(Vampire &to);
	virtual void specialEffect(Goblin &to);
	virtual void specialEffect(Drow &to);
	virtual void specialEffect(Elf &to);
	virtual void specialEffect(Dwarf &to);
	//execute only if none of the above methods can be used
	virtual void specialEffect(Character &to);
	//enable double dispatch
	void receiveEffect(Character &from);
	//char used to represent character in text display - see CC3K specifications to know the avatars for each race
	virtual char getAvatar();
	virtual std::string attack(Character &who); //overloaded for special attacks
	//for special on hit effects
	virtual void takeHit(Vampire &from);
	virtual void takeHit(Goblin &from);
	virtual void takeHit(Elf &from);
	//not for double dispatch, but for damage calculation
	virtual void takeHit(Character &from);
	//virtual void takeHit(Orc &from);
	Stats &getStats();
	Stats &getBaseStats();
	std::string statsString();
	void changeStats(Stats &newS);
	void changeCell(Cell *whatCell);
	virtual void die();
	virtual void move();
	Coordinate getPos();
	virtual bool isPlayable();
	bool getActive();
	virtual void setInactive(); //basically turns off this enemy
	virtual int getScore();
	virtual void selfPulseEvent();
};

#endif
