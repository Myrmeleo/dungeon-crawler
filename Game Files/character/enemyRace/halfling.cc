#include "halfling.h"
#include <cstdlib>

Halfling::Halfling(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(100, 100, 15, 20)), *(new Stats(100, 100, 15, 20)), p, c)
{}

void Halfling::takeHit(Vampire &from) {
	int hitChance = rand() % 2;
	if (hitChance == 0) {
		double c100 = 100;
		int dmg = ceil(c100 / (c100 + getStats().getDEF()) * from.getStats().getATK());
		getStats().addHP(-dmg);
		from.specialEffect(*this);
		if (stats->getHP() < 1) this->die();
	}
}

void Halfling::takeHit(Character &from) {
	int hitChance = rand() % 2;
	if (hitChance == 0) {
		double c100 = 100;
		int dmg = ceil(c100 / (c100 + getStats().getDEF()) * from.getStats().getATK());
		getStats().addHP(-dmg);
		if(stats->getHP() < 1) this->die();
	}
}

char Halfling::getAvatar() {
	return 'L';
}
