#ifndef ELF_H
#define ELF_H
#include "enemyrace.h"


class Elf : public EnemyRace {
public:
	Elf(Coordinate &p, Cell &c);
	char getAvatar() override;
	void specialEffect(Character &to) override;
	std::string attack(Character &who) override;
};


#endif
