#include "human.h"
#include "../../item/gold/small.h"
#include "../../item/gold/normal.h"

Human::Human(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(140, 140, 20, 20)), *(new Stats(140, 140, 20, 20)), p, c)
{}

char Human::getAvatar() {
	return 'H';
}

void Human::die(){
	//shared part
	//hack to get around reference
	Coordinate tmp = getPos();
	
	Normal *drop = new Normal(tmp, *cell);
	cell->setItem(drop);
	cell->killCharacter();
	
	Cell *neighbourCell = cell->getRandomNeighbour();
	int timer = 1;
	while(neighbourCell->isOccupied()){
		neighbourCell = cell->getRandomNeighbour();
		//shouldn't ever run into this problem
		++timer;
		if (timer > 100) { break; }
	}
	if (!neighbourCell->isOccupied()) {
		Normal *drop2 = new Normal(tmp, *cell);
		neighbourCell->setItem(drop2);
	}
	this->setInactive();
}
