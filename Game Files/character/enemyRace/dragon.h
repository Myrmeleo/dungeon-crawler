#ifndef DRAGON_H
#define DRAGON_H
#include "enemyrace.h"
//#include "../../item/gold/dhoard.h"

class DragonHoard;

class Dragon : public EnemyRace {
	//assign each dragon a special gold stash
	DragonHoard *stash;
public:
	Dragon(Coordinate &p, Cell &c, DragonHoard &dh);
	char getAvatar() override;
	//this should do nothing
	void move() override;

	//needs to also delete references to gold
	void setInactive() override;

	std::string attack(Character &who) override;

	void die() override;
};


#endif
