#include "dragon.h"
#include "../../item/gold/dhoard.h"

Dragon::Dragon(Coordinate &p, Cell &c, DragonHoard &dh) : 
	EnemyRace(*(new Stats(150, 150, 20, 20)), *(new Stats(150, 150, 20, 20)), p, c), stash(&dh) 
{
	dh.addGuardian(*this);
}

char Dragon::getAvatar() {
	return 'D';
}

void Dragon::move() {
	//this is supposed to do nothing
}

void Dragon::setInactive(){
	cell->killCharacter();
	cell = nullptr;
	active = false;
	
	//dragon unique stuff
	stash->notifyDeath();
	stash = nullptr;
	
}

void Dragon::die(){
	setInactive();
}

std::string Dragon::attack(Character &who){
	//if something is next to the dragon or its hoard attack it
	if(cell){
		if(oneTileAway(who.getPos(), getPos()) || oneTileAway(who.getPos(), stash->getPos())){
			
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
		}
	}
	return "";
}
