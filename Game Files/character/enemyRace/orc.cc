#include "orc.h"
#include "../playerRace/goblin.h"
#include<iostream>
Orc::Orc(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(180, 180, 30, 25)), *(new Stats(180, 180, 30, 25)), p, c)
{}

void Orc::specialEffect(Goblin &to){
	
	
	if(!(to.getActive())) return; //no need to add damage to the dead
	
	double c100 = 100;
	int dmg = ceil(c100 / (c100 + getStats().getDEF()) * to.getStats().getATK());
	stats->addHP(-dmg / 2); //~same as multiplying by 0.5
	
	if(stats->getHP() < 1) die();
}

char Orc::getAvatar() {
	return 'O';
}

using namespace std;
std::string Orc::attack(Character &who){
	
	if(cell){
		if(oneTileAway(who.getPos(), getPos())){
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
		}
	}
	return "";
}

