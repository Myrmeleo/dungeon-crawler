#include "dwarf.h"
#include "../playerRace/vampire.h"
Dwarf::Dwarf(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(100, 100, 20, 30)), *(new Stats(100, 100, 20, 30)), p, c)
{}

char Dwarf::getAvatar() {
	return 'W';
}


void Dwarf::takeHit(Vampire &from){
	damageTaken(from);
	from.specialEffect(*this);
}
