#ifndef DWARF_H
#define DWARF_H
#include "enemyrace.h"


class Dwarf : public EnemyRace {
public:
	Dwarf(Coordinate &p, Cell &c);
	char getAvatar() override;
	void takeHit(Vampire &from);
};


#endif
