#include "elf.h"

Elf::Elf(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(140, 140, 30, 10)), *(new Stats(140, 140, 30, 10)), p, c)
{}

void Elf::specialEffect(Character &to) {
	to.takeHit(*static_cast<Character*>(this));
}

char Elf::getAvatar() {
	return 'E';
}

std::string Elf::attack(Character &who) {
	
	
	if(cell){
		if(oneTileAway(who.getPos(), getPos())){
			
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
		}
	}
	return "";
}
