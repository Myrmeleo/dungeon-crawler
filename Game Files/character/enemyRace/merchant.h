#ifndef MERCHANT_H
#define MERCHANT_H
#include "enemyrace.h"


class Merchant : public EnemyRace {
	static bool hostile;
public:
	Merchant(Coordinate &p, Cell &c);
	static bool isHostile();
	static void becomeHostile();
	static void resetHostility();
	//make merchants hostile
	void takeHit(Character &from) override;
	char getAvatar() override;

	std::string attack(Character &who) override;

	void die() override;
};


#endif
