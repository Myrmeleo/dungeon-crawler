#include "merchant.h"
#include "../../item/gold/mhoard.h"
#include <math.h>

bool Merchant::hostile = false;

Merchant::Merchant(Coordinate &p, Cell &c) : EnemyRace(*(new Stats(30, 30, 70, 5)), *(new Stats(30, 30, 70, 5)), p, c)
{}

bool Merchant::isHostile() {
	return hostile;
}

void Merchant::becomeHostile() {
	hostile = true;
}

void Merchant::resetHostility() {
	hostile = false;
}

void Merchant::takeHit(Character &from) {
	becomeHostile();
	double c100 = 100;
	int dmg = ceil(c100 / (c100 + getStats().getDEF()) * from.getStats().getATK());
	getStats().addHP(-dmg);
	
	if(getStats().getHP() < 1) this->die();
}

char Merchant::getAvatar() {
	return 'M';
}

std::string Merchant::attack(Character &who){
	if(isHostile()){
		if(oneTileAway(who.getPos(), getPos())){
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
		}
	}
	return "";
}

void Merchant::die(){
	Coordinate tmp = getPos();
	MerchHoard *drop = new MerchHoard(tmp, *cell);
	cell->setItem(drop);
	cell->killCharacter();
	setInactive();
}
