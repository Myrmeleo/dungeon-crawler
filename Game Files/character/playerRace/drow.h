#ifndef DROW_H
#define DROW_H
#include "playerrace.h"


class Drow : public PlayerRace {
public:
	Drow(Coordinate &p, Cell &c);

	std::string usePot(std::string dir) override;

	void takeHit(Elf &from) override;
};


#endif
