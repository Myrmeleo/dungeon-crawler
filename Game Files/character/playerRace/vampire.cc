#include "vampire.h"
#include "../enemyRace/dwarf.h"

Vampire::Vampire(Coordinate &p, Cell &c) : 
	PlayerRace(*(new Stats(50, 10000, 25, 25)), *(new Stats(50, 10000, 25, 25)), p, c)
{}


std::string Vampire::attack(Character &who) {
	
	if(cell){
		if(oneTileAway(who.getPos(), getPos())){
			
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
		}
	}
	return "";
}

void Vampire::specialEffect(Character &to) {
	getStats().addHP(5);
}

void Vampire::specialEffect(Dwarf &to) {
	getStats().addHP(-5);
}
