#include "playerrace.h"
#include "../../item/item.h"
#include <cstdlib>
//#include<iostream>

using namespace std;

PlayerRace::PlayerRace(Stats &s, Stats &bs, Coordinate &p, Cell &c) : Character(s, bs, p, c)
{}

PlayerRace::~PlayerRace() {
}

void PlayerRace::takeHit(Character &from) {
	int hitChance = rand() % 2;
	if (hitChance == 0) {
		double c100 = 100;
		int dmg = ceil(c100 / (c100 + getStats().getDEF()) * from.getStats().getATK());
		
//		dmg = 0;
//		printStats();
//		cout<<"lijhubahfbalhsbdlfbasdlbvflasd"	<<endl;
		getStats().addHP(-dmg);
	}
	if(getStats().getHP() < 1) setInactive();
	
	
}

char PlayerRace::getAvatar() {
	return '@';
}

std::string PlayerRace::move(string dir){
	cell->send(dir);
	std::string out = "You move ";
	if (dir == "no") {
		out += "north";
	}
	else if (dir == "ne") {
		out += "northeast";
	}
	else if (dir == "ea") {
		out += "east";
	}
	else if (dir == "se") {
		out += "southeast";
	}
	else if (dir == "so") {
		out += "south";
	}
	else if (dir == "sw") {
		out += "southwest";
	}
	else if (dir == "we") {
		out += "west";
	}
	else if (dir == "nw") {
		out += "northwest";
	}
	Item *inFront = cell->getItemInDir(dir);
	if (inFront) {
		out += " and see " + inFront->giveType();
	}
	out += ".";
	return out;
}

Cell *PlayerRace::getCell(){
	return cell;
}

void PlayerRace::setCell(Cell *newCell){
	cell = newCell;
}

bool PlayerRace::isPlayable(){
	return true;
}

std::string PlayerRace::usePot(std::string dir){
	Item *item = cell->getItemInDir(dir);
	if (item) {
		item->myEffect(*this);
		if (getStats().getHP() < 1) {
			getStats().addHP(1);
		}
		return item->describeEffect();
	}
	return "Nothing happens, because there is nothing in front of you.";
}

std::string PlayerRace::attackInDir(string dir){
	Character *character = cell->getCharacterInDir(dir);
	if(character){
		return this->attack(*character);
	}
	return "You swing at the air.";
}

void PlayerRace::remStatEffects(){
	int gold = stats->getGold();
	int HP = stats->getHP();
	
	delete stats;
	stats = new Stats(*baseStats);
	
	stats->addGold(gold - stats->getGold());
	stats->addHP(HP - stats->getHP());
}
