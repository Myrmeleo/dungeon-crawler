#include "troll.h"


Troll::Troll(Coordinate &p, Cell &c) : 
	PlayerRace(*(new Stats(120, 120, 25, 25)), *(new Stats(120, 120, 25, 25)), p, c)
{}

void Troll::selfPulseEvent(){
	getStats().addHP(5);
}
