#include "goblin.h"
#include "../enemyRace/orc.h"

Goblin::Goblin(Coordinate &p, Cell &c) : 
	PlayerRace(*(new Stats(110, 110, 15, 20)), *(new Stats(110, 110, 15, 20)), p, c)
{}

std::string Goblin::attack(Character &who) {
	
	if(cell){
		if(oneTileAway(who.getPos(), getPos())){
			
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString();
			
		}
	}
	return "";
}

void Goblin::specialEffect(Character &to){
	if(!(to.getActive())) getStats().addGold(5);
}

void Goblin::takeHit(Orc &from){
	damageTaken(from);
	
	from.specialEffect(*this);
	
}
