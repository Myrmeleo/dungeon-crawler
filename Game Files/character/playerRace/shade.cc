#include "shade.h"


Shade::Shade(Coordinate &p, Cell &c) : 
	//atk 25 def 25
	PlayerRace(*(new Stats(125, 125, 25, 25)), *(new Stats(125, 125, 25, 25)), p, c)
{}

int Shade::getScore(){
	int score = getStats().getGold();
	return score + score/2; //a different way to add 50%
}
