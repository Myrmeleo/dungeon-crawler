#ifndef GOBLIN_H
#define GOBLIN_H
#include "playerrace.h"


class Goblin : public PlayerRace {
public:
	Goblin(Coordinate &p, Cell &c);
	std::string attack(Character &who) override;
	void specialEffect(Character &to);
	void takeHit(Orc &from);
};


#endif
