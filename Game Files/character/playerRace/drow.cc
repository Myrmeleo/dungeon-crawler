#include "drow.h"
#include "../../item/item.h"
#include <math.h>
#include "../enemyRace/elf.h"
Drow::Drow(Coordinate &p, Cell &c) : 
	PlayerRace(*(new Stats(150, 150, 25, 15)), *(new Stats(150, 150, 25, 15)), p, c)
{}

Stats *ChangeInStats(Stats latter, Stats former){
	int HP = latter.getHP() - former.getHP();
	int ATK = latter.getATK() - former.getATK();
	int DEF = latter.getDEF() - former.getDEF();
	
	return new Stats(HP, 0, ATK, DEF);
}
std::string Drow::usePot(std::string dir){
	Item *item = cell->getItemInDir(dir);
	
	if(item){
		//max HP not compared
		Stats origStats{getStats().getHP(), 0,getStats().getATK(), getStats().getDEF()};
		item->myEffect(*this);
		
		
		//special effect
		Stats *diffStats = ChangeInStats(getStats(), origStats);
		
		getStats().addATK(round((double)diffStats->getATK() * 0.5));
		getStats().addDEF(round((double)diffStats->getDEF() * 0.5));
		getStats().addHP(round((double)diffStats->getHP() * 0.5));
		
		delete diffStats;
		
		if(getStats().getHP() < 1){
			getStats().addHP(1);
		}
		return item->describeEffect();
	}
	return "Nothing happens, because there is nothing in front of you.";
}

void Drow::takeHit(Elf &from){
	damageTaken(from);
	
	//no special effect as spec
}

