#ifndef VAMPIRE_H
#define VAMPIRE_H
#include "playerrace.h"

class Dwarf;

class Vampire : public PlayerRace {
public:
	Vampire(Coordinate &p, Cell &c);

	std::string attack(Character &who) override;
	void specialEffect(Character &to) override;
	void specialEffect(Dwarf &to) override;
};


#endif
