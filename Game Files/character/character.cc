#include "character.h"
#include "../item/gold/small.h"
#include "../item/gold/normal.h"
#include <cstdlib>
#include <string>


#include "enemyRace/dragon.h"
#include "enemyRace/dwarf.h"
#include "enemyRace/elf.h"
#include "enemyRace/halfling.h"
#include "enemyRace/human.h"
#include "enemyRace/merchant.h"
#include "enemyRace/orc.h"


#include "playerRace/drow.h"
#include "playerRace/goblin.h"
#include "playerRace/troll.h"
#include "playerRace/vampire.h"
#include "playerRace/shade.h"

#include<iostream>

using namespace std;

string whatDir(){
	
	int whichDir = rand() % 8;
	if(whichDir == 0){
		return "no";
	}
	else if(whichDir == 1){
		return "so";
	}
	else if(whichDir == 2){
		return "ea";
	}
	else if(whichDir == 3){
		return "we";
	}
	else if(whichDir == 4){
		return "ne";
	}
	else if(whichDir == 5){
		return "nw";
	}
	else if(whichDir == 6){
		return "se";
	}
	else if(whichDir == 7){
		return "sw";
	}
	
	//should never reach here
	return "";
	
}

Character::Character(Stats &s, Stats &bs, Coordinate &p, Cell &c) : position(&p), stats(&s), baseStats(&bs), cell(&c)
{}

Character::~Character() {
	delete stats;
	delete baseStats;
}
void Character::specialEffect(Character &to){
	; //actually very important. overload this func for on hit effects
}

void Character::specialEffect(Vampire &to) {
	to.getStats().addHP(5);
}

void Character::specialEffect(Goblin &to) {
	
	if(!getActive()) to.getStats().addGold(5);
}

void Character::specialEffect(Drow &to) {
	(void)to;
}
void Character::specialEffect(Dwarf &to){
	
}
void Character::specialEffect(Elf &to) {
	//use the standard attack instead of elf attack
	//static_cast<Character &>(to).attack(*this);
}

void Character::receiveEffect(Character &from) {
	from.specialEffect(*this);
}

char Character::getAvatar() {
	return '.';
}

std::string Character::attack(Character &who) {
	if(cell){
		if(oneTileAway(who.getPos(), getPos())){
			who.takeHit(*this);
			return statsString() + " attacks " + who.statsString() + "\n";
		}
	}
	return "";
}

void Character::damageTaken(Character &from){
	double c100 = 100;
	int dmg = ceil(c100 / (c100 + stats->getDEF()) * from.getStats().getATK());
	stats->addHP(-dmg);
	
	if(stats->getHP() < 1) die();
}
void Character::takeHit(Character &from) {
	
	damageTaken(from);
	
}

void Character::takeHit(Vampire &from){
	damageTaken(from);
	
	cout<<endl;
	
	from.specialEffect(*this);
}

void Character::takeHit(Goblin &from){
	damageTaken(from);
	
	
	from.specialEffect(*this);
}
void Character::takeHit(Elf &from){
	
	damageTaken(from);
	
	if (getActive()) {
		from.specialEffect(*this);
	}
}
/*
void Character::takeHit(Orc &from){
	damageTaken(from);
	
	printStats();
	cout<<"attacked by6: ";
	from.printStats();
	cout<<endl;
}*/

Stats &Character::getStats() {
	return *stats;
}

Stats &Character::getBaseStats() {
	return *baseStats;
}

void Character::changeStats(Stats &newS) {
	delete stats;
	stats = &newS;
}

void Character::changeCell(Cell *whatCell){
	cell = whatCell;
}

void Character::die(){
	//hack to get around reference
	Coordinate tmp = getPos();
	
	int whichDrop = rand() % 2;
	if (whichDrop == 0) {
		Small *drop = new Small(tmp, *cell);
		cell->setItem(drop);
		cell->killCharacter();
	} else {
		Normal *drop = new Normal(tmp, *cell);
		cell->setItem(drop);
		cell->killCharacter();
	}
	
	this->setInactive();
}

void Character::move(){

	string dir = whatDir();
	if(cell){
		cell->send(dir);
		//position = cell->getPos();
	}
}

Coordinate Character::getPos(){
	if(cell) return *(cell->getPos());
	else return Coordinate{-10, -10};
}

std::string Character::statsString(){
	std::string avatar(1, getAvatar());
	return avatar + " -> | " +
	"Health: " + to_string(stats->getHP()) + "/" + to_string(stats->getMaxHP()) + " | " +
	"Attack: " + to_string(stats->getATK()) + " | " +
	"Defense: " + to_string(stats->getDEF()) + " | " +
	"Gold: " + to_string(stats->getGold()) + " | ";
}

bool Character::isPlayable(){
	return false;
}

bool Character::getActive(){
	return active;
}

void Character::setInactive(){
	//remove references
	cell->killCharacter();
	cell = nullptr;
	position = nullptr;
	
	active = false;
}
	
int Character::getScore(){
	return stats->getGold();
}

void Character::selfPulseEvent(){
	
}
