#include <iostream>
#include "dungeon.h"
#include <string>
//may put in sep func
#include "character/playerRace/drow.h"
#include "character/playerRace/goblin.h"
#include "character/playerRace/troll.h"
#include "character/playerRace/vampire.h"
#include "character/playerRace/shade.h"
#include "character/enemyRace/merchant.h"
#include "coordinate.h"

using namespace std;

bool topWallBottomWall(const string & a);

Dungeon::Dungeon(ifstream &mapFile, bool whatGen) :randGen{ whatGen } {
	//clears screen for future
	system("cls");

	string floorLine;
	int i = 0;

	//load a map File in
	while (getline(mapFile, floorLine)) {

		if (topWallBottomWall(floorLine)) {
			FloorMaps.emplace_back(vector<string>());
			FloorMaps.at(i).emplace_back(floorLine);

			getline(mapFile, floorLine);

			int j = 1;
			while (!topWallBottomWall(floorLine)) {
				FloorMaps.at(i).emplace_back(floorLine);
				getline(mapFile, floorLine);

				++j;
			}
			FloorMaps.at(i).emplace_back(floorLine);
			++i;
		}
	}
	level = 0;
	td = nullptr;
	currentFloor = nullptr;
	player = nullptr;
	nextFloorLoc = nullptr;
	reset();
}

Dungeon::~Dungeon() {
	delete td;
	delete currentFloor;
	delete nextFloorLoc;
	delete player;
}

bool validSelect(string selection){
	return selection == "d" || selection == "v" || selection == "t" || 
			selection == "g" || selection == "s";
}

void Dungeon::characterSelect(){
	delete player;
	cout<<"Welcome to CC3k!"<<endl;
	cout<<"Select your character:\n";
	cout<<"Drow - potions are 50% stronger: d"<<endl;
	cout<<"Vampire - heals 5HP on attack, high upper HP limit: v"<<endl;
	cout<<"Troll - heals 5HP per turn: t"<<endl;
	cout<<"Goblin - steals 5 gold from slain enemies: g"<<endl;
	cout<<"Shade - 50% higher score: s"<<endl;
	string selection = "";
	while(!validSelect(selection)){
		cout<<"Choice: ";
		cin>>selection; //assume no need to press q
	}
	Coordinate tmpCoord{-1, -1};
	Cell tmpCell;
	//note coordinates filled in later
	if(selection == "d"){
		player = new Drow(tmpCoord, tmpCell);
	}
	else if(selection == "v"){
		player = new Vampire(tmpCoord, tmpCell);
	}
	else if(selection == "t"){
		player = new Troll(tmpCoord, tmpCell);
	}
	else if(selection == "g"){
		player = new Goblin(tmpCoord, tmpCell);
	}
	else{// selection == "s"
		player = new Shade(tmpCoord, tmpCell);
	}
}

bool topWallBottomWall(const string & a){
	int i = 1;
	int end = a.size();
	
	if(a[0] != '|') return false;
	if(a[end - 1] != '|') return false;
	
	while(i < end - 1){
		if(a[i] != '-'){
			return false;
		}
		++i;
	}
	
	return true;
}

//determines if the cmd was a direction
bool isDirection(string cmd){
	return cmd == "no" ||cmd == "so" || cmd == "ea" || cmd == "we" || 
			cmd == "ne" || cmd == "nw" || cmd == "se" || cmd == "sw";
}

bool Dungeon::play(){
	string cmd = "";
	string lastGamestate = "";
	string gamestateUpdate = "";
	system("cls");
	bool updatePieces;
	bool freeze = false;
	int i = 1;
	while(cmd != "q"){
		lastGamestate = gamestateUpdate;
		gamestateUpdate = "";
		updatePieces = true;
		lastGamestate += currentFloor->refresh();
		td->print();
		cout << player->statsString() << "Turn: " << i << " | Floor: " << level << endl;
		cout << "Coordinates: ( " << player->getPos().getX() << ", " << player->getPos().getY() << " )" << endl;
		cout << lastGamestate << endl;
		cout << "Enter Command: ";
		cin >> cmd;
		
		if (isDirection(cmd)){
			gamestateUpdate += player->move(cmd) + "\n";
			
		}
		else if (cmd == "u"){ //use potion
			cin >> cmd;
			if (isDirection(cmd)) {
				gamestateUpdate += player->usePot(cmd) + "\n";
			}
		}
		else if (cmd == "a"){ //attack
			cin >> cmd;
			if (isDirection(cmd)) {
				gamestateUpdate += player->attackInDir(cmd) + "\n";
			}
		}
		else if (cmd == "f"){
			freeze = !freeze;
			gamestateUpdate += "ZA WARUDO!\n";
		}
		else if (cmd == "r"){
			gamestateUpdate = "";
			reset();
			updatePieces = false;
			i = 0;
		}
		else if (cmd == "help") {
			gamestateUpdate += gameManual();
		}
		else {
			gamestateUpdate += "Invalid command. For a list of instructions, enter \"help\".\n";
		}
		
		
		if(updatePieces){
			gamestateUpdate += currentFloor->reactEnemies(player) + "\n";
			if(!freeze) moveEnemies();
			player->selfPulseEvent(); //activates troll special
		}
		
		//check if dead after all enemies react
		if(!(player->getActive())){
			victorySplash();
			cmd = "";
			while(cmd != "y" && cmd !="n"){
				cout << "would you like to continue? (y or n):";
				cin>>cmd;
			}
			
			if(cmd == "y"){
				reset();
			}
			else{
				return true;
			}
		}
		if(player->getPos() == *nextFloorLoc){
			if(level + 1 == FloorMaps.size() + 1){
				victorySplash();
				cmd = "";
				while(cmd != "y" && cmd !="n"){
					cout << "would you like to continue? (y or n):";
					cin>>cmd;
				}
				
				if(cmd == "y"){
					reset();
				}
				else{
					return true;
				}
			}
			//else head to the next floor
			else{
				nextFloor();
			}
		}
		system("cls");
		++i;
	}
	cout << "Ending the game. Goodbye. " << endl;
	return true;
}

string Dungeon::gameManual() {
	string theManual = "";
	theManual += "Welcome to CC3K!\n";
	theManual += "Can you make it through all 5 floors?\n";
	theManual += "\n";
	theManual += "Game entities are represented by one of the following characters:\n";
	theManual += "@ - the player. Dodges 50% of enemy attacks.\n";
	theManual += "H - an enemy human. Drops two piles of gold, not just one.\n";
	theManual += "W - an enemy dwarf. When attacked by a vampire, that vampire loses 5HP.\n";
	theManual += "E - an enemy elf. Attacks twice against every race except a drow.\n";
	theManual += "O - an enemy orc. Deals 50% more damage to goblins.\n";
	theManual += "L - an enemy orc. Dodges 50% of attacks (other enemies never dodge).\n";
	theManual += "D - an enemy dragon. Never moves away from its gold pile.\n";
	theManual += "M - a merchant. Non-hostile until attacked, at which point all merchants are hostile for the rest of the game.\n";
	theManual += "G - a gold pile. Comes in one of four sizes.\n";
	theManual += "P - a potion. Effect unknown until consumed, at which point it becomes known for the rest of the game.\n";
	theManual += "\n";
	theManual += "Inputs must be one of the following:\n";
	theManual += "[any of no, ea, so, we, ne, se, sw, nw] - move one unit in any of the eight possible directions.\n";
	theManual += "u [any of no, ea, so, we, ne, se, sw, nw] - use a potion one unit away in the given direction.\n";
	theManual += "a [any of no, ea, so, we, ne, se, sw, nw] - attack an enemy one unit away in the given direction.\n";
	theManual += "f - freeze all enemies until entered a second time.\n";
	theManual += "r - reset the game, including a new race selection.\n";
	theManual += "q - quit the game.\n";
	return theManual;
}

void Dungeon::moveEnemies() {
	currentFloor->moveEnemies();
}

void Dungeon::nextFloor() {
	delete td;
	td = new TextDisplay(FloorMaps.at(level));
	delete currentFloor;
	//setup new floor
	currentFloor = new Floor(FloorMaps.at(level), td, randGen, player, nextFloorLoc);
	delete nextFloorLoc;
	nextFloorLoc = currentFloor->getStairLoc();
	player->remStatEffects();
	++level;
}

void Dungeon::victorySplash(){
	system("cls");
	if(player->getActive()){
		cout << "***************************************************************" << endl;
		cout << "*******************  YAY YOU WON CONGRATS  ********************" << endl;
		cout << "***************************************************************" << endl;
	}
	else{
		cout << "***************************************************************" << endl;
		cout << "********************  OH NO YOU DIED RIP  *********************" << endl;
		cout << "***************************************************************" << endl;
	}
	cout<<"You scored: "<<player->getScore()<<endl;
}

void Dungeon::reset(){
	delete td;
	td = new TextDisplay(FloorMaps.at(0));
	//selects player here
	characterSelect();
	//let floor handle placing player and nextFloorLoc
	delete currentFloor;
	currentFloor = new Floor(FloorMaps.at(0), td, randGen, player, nextFloorLoc);
	delete nextFloorLoc;
	nextFloorLoc = currentFloor->getStairLoc();
	Merchant::resetHostility();
	level = 1;
}
